using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Core.ServerResponse
{
    public class ResultMessage
    {
        public static string Success => "messages.success.general.ok";
        public static string Error => "messages.error.general.false";
        public static string InvalidModel => "messages.error.general.invalidModel";
        public static string UnhandledException => "messages.error.general.unhandledException";
        public static string UnAuthorized => "messages.error.general.unAuthorized";
        public static string Forbidden => "messages.error.general.Forbidden";

        // Customer
        public static string NotFoundCustomer => "messages.error.customer.notFoundCustomer";

        // Product
        public static string NotFoundProduct => "messages.error.product.notFoundProduct";

        // Order
        public static string NotFoundOrder => "messages.error.order.notFoundOrder";
    }
}